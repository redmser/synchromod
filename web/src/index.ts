import express from 'express';
import http from 'http';
import https from 'https';
import { Config } from './config.js';
import router from './router.js';

const app = express();
const config: Config = {} as any;

app.use('/api', router);
// TODO: Static hosted content here too

let server: http.Server | https.Server;
let port: number;
if (config.useHTTPS !== true /* false or undefined */) {
    // HTTP
    server = http.createServer(app);
    port = config.port || 80;
} else {
    // HTTPS
    server = https.createServer({ cert: config.cert, key: config.key }, app);
    port = config.port || 443;
}

server.listen(port, () => {
    console.log(`${config.useHTTPS !== true ? 'HTTP' : 'HTTPS'} server listening on port ${port}`);
});
