export type Config = ConfigBase & (ConfigHTTP | ConfigHTTPS);

export interface ConfigBase {
    port?: number;
    useHTTPS?: boolean;
}

export interface ConfigHTTP {
    useHTTPS: false;
}

export interface ConfigHTTPS {
    useHTTPS: true;
    cert: string;
    key: string;
}
