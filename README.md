# Compiling

- `git clone` this repository
- Global dependency: `npm i --global gulp-cli`
- In project root + in every project folder that should be compiled (bot|web): `npm i`
- In project root: `npm run build` to prod build all projects, or `npm run build:(bot|web)` for a specific project
- In project root: For dev builds, use `npm run build:dev` or `npm run watch`
- Check gulp file for more specific build pipelines
