import gulp from 'gulp';
import tsGulp from 'gulp-typescript';
import { exec, spawn } from 'child_process';
import { promisify } from 'util';
import { unlink, rmdir } from 'fs';

type ProjectName = 'bot' | 'web';

const execAsync = promisify(exec);
const deleteAsync = promisify(unlink);
const rmdirAsync = promisify(rmdir);


const tsProjectBot = tsGulp.createProject("tsconfig.json", {
    rootDir: "./bot/src/"
});

const tsProjectWeb = tsGulp.createProject("tsconfig.json", {
    rootDir: "./web/src/"
});


/**
 * Clean dist folder before compile (should be done before any prod build, or for clean dev builds).
 */
const cleanPre = async () => {
    await rmdirAsync('./dist', { recursive: true });
};
exports.clean = cleanPre;

/**
 * Clean dist folder after compile (should be done automatically at the end of any prod build).
 */
const cleanPost = async () => {
    // Delete package-lock.json
    await deleteAsync('./dist/bot/package-lock.json');
    await deleteAsync('./dist/web/package-lock.json');
};


/**
 * Build project dependencies.
 */
function buildDeps(name: ProjectName): gulp.TaskFunction {
    return async () => {
        // Copy package.json
        await new Promise((resolve, reject) => {
            gulp.src(`${name}/package.json`)
                .pipe(gulp.dest(`dist/${name}/`))
                .on('error', reject)
                .on('end', resolve);
        });

        // Run npm install
        await execAsync('npm install --only=prod', { cwd: `./dist/${name}/` });

        // Delete package.json (keep package-lock.json for performance)
        await deleteAsync(`./dist/${name}/package.json`);
    };
}

/**
 * Build dependencies of bot.
 */
const buildBotDeps = buildDeps('bot');

/**
 * Build dependencies of web.
 */
const buildWebDeps = buildDeps('web');


/**
 * Build a project by name.
 */
function buildProject(name: ProjectName, project: tsGulp.Project): gulp.TaskFunction {
    return () => {
        const tsResult = gulp.src(`./${name}/src/**/*.ts`)
            .pipe(project());
        return tsResult.js
            .pipe(gulp.dest(`dist/${name}`));
    };
}

/**
 * Dev build of bot.
 */
const buildBotDev = buildProject('bot', tsProjectBot);
exports.buildBotDev = gulp.parallel(buildBotDev, buildBotDeps);

/**
 * Prod build of bot.
 */
const buildBotProd = gulp.series(cleanPre, buildBotDev, cleanPost);
exports.buildBot = buildBotProd;

/**
 * Dev build of web.
 */
const buildWebDev = buildProject('web', tsProjectWeb);
exports.buildWebDev = gulp.parallel(buildWebDev, buildWebDeps);

/**
 * Prod build of web.
 */
const buildWebProd = gulp.series(cleanPre, buildWebDev, cleanPost);
exports.buildWeb = buildWebProd;

/**
 * Dev build of all projects.
 */
const buildDev = gulp.parallel(buildBotDev, buildBotDeps, buildWebDev, buildWebDeps);
exports.buildDev = buildDev;

/**
 * Prod build of all projects.
 */
const buildProd = gulp.series(cleanPre, buildDev, cleanPost);
exports.build = buildProd;

/**
 * Watch dev build of all projects.
 */
const watchDev = () => {
    gulp.watch(["./bot/**/*", "./web/**/*"], { ignoreInitial: false }, buildDev);
};
exports.watch = watchDev;


/**
 * Start a named project.
 */
function startProject(name: ProjectName): gulp.TaskFunction {
    return () => spawn('node', ['index.js'], { cwd: `./dist/${name}/`, stdio: 'inherit' });
}

/**
 * Start bot project.
 */
const startBot = startProject('bot');
exports.startBot = startBot;

/**
 * Start web project.
 */
const startWeb = startProject('web');
exports.startWeb = startWeb;

/**
 * Start all projects in parallel.
 */
const startAll = gulp.parallel(startBot, startWeb);
exports.startAll = startAll;
